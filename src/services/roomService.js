import axios from "axios";
import { BASE_URL, createConfig } from "./configURL";

export const roomService = {
  getRoomList: (data) => {
    return axios({
      url: `${BASE_URL}/api/phong-thue`,
      method: "GET",
      data: data,
      headers: createConfig(),
    });
  },
};
