import axios from "axios";
import { BASE_URL, createConfig } from "./configURL";

export const userService = {
  postDangNhap: (data) => {
    return axios({
      url: `${BASE_URL}/api/auth/signin`,
      method: "POST",
      data: data,
      headers: createConfig(),
    });
  },
  postDangKy: (data) => {
    return axios({
      url: `${BASE_URL}/api/auth/signup`,
      method: "POST",
      data: data,
      headers: createConfig(),
    });
  },
};
