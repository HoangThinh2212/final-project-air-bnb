import React from "react";
import { Button, Form, Input, message } from "antd";
import { userService } from "./../../services/userService";
import Lottie from "lottie-react";
import animate_login from "../../assets/login.json";
import background from "../../assets/background_login.jpg";
import { Link, useNavigate } from "react-router-dom";

export default function LoginPage() {
  let navigate = useNavigate();
  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .postDangNhap(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng nhập thành công");
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Đã có lỗi xảy ra");
        console.log(err);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div
      className="w-screen h-screen flex justify-center items-center "
      style={{
        backgroundImage: `url(${background})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
    >
      <div className="container p-5 flex relative">
        <div className="h-full w-2/3">
          <Lottie animationData={animate_login} loop={true} />
        </div>
        <div className="h-full w-2/3 relative lg:top-28 right-2 xl:top-56">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập email!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Mật khẩu"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 2,
                span: 12,
              }}
            >
              <Button className="bg-teal-400 text-black mr-5" htmlType="submit">
                Đăng nhập
              </Button>

              <Button className="bg-pink-400 text-black">
                <Link to="/register" style={{ textDecoration: "none" }}>
                  
                  Đăng ký
                </Link>
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
