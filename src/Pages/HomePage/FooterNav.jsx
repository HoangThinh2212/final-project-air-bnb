import React from "react";

export default function FooterNav() {
  return (
    <div className="bg-gray-50 border-t fixed bottom-0 w-screen z-10 py-4 hidden md:block">
      <div className="container mx-auto flex justify-between text-gray-500 ">
        <div className="flex gap-3 text-gray-500 font-medium">
          <p> © 2022 Airbnb, Inc.</p>
          <li>
            <a className="hover:text-gray-400" href="/">
              Quyền riêng tư
            </a>
          </li>
          <li>
            <a className="hover:text-gray-400" href="/">
              Điều khoản
            </a>
          </li>
          <li>
            <a className="hover:text-gray-400" href="/">
              Sơ đồ trang web
            </a>
          </li>
        </div>
        <div className="flex text-gray-500 items-center">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M12 21a9.004 9.004 0 008.716-6.747M12 21a9.004 9.004 0 01-8.716-6.747M12 21c2.485 0 4.5-4.03 4.5-9S14.485 3 12 3m0 18c-2.485 0-4.5-4.03-4.5-9S9.515 3 12 3m0 0a8.997 8.997 0 017.843 4.582M12 3a8.997 8.997 0 00-7.843 4.582m15.686 0A11.953 11.953 0 0112 10.5c-2.998 0-5.74-1.1-7.843-2.918m15.686 0A8.959 8.959 0 0121 12c0 .778-.099 1.533-.284 2.253m0 0A17.919 17.919 0 0112 16.5c-3.162 0-6.133-.815-8.716-2.247m0 0A9.015 9.015 0 013 12c0-1.605.42-3.113 1.157-4.418"
            />
          </svg>
          <div>
            <span className="hover:underline cursor-pointer font-medium">
              Tiếng Việt(VN)
            </span>
            <i className="fa fa-dollar-sign font-medium cursor-pointer pl-3" />
            <span className="hover:underline cursor-pointer px-1 font-medium">
              USD
            </span>
            <span className="px-3">
              <i className="fa-brands fa-facebook-f px-2" />
              <i className="fa-brands fa-twitter px-2 " />
              <i className="fa-brands fa-instagram px-2" />
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}
