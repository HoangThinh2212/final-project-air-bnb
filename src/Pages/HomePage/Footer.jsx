import React from "react";

export default function Footer() {
  return (
    <div className="border-t mt-5 pt-5 bg-gray-100">
      <div className="container grid grid-cols-2 mx-auto gap-x-3 gap-y-8 sm:grid-cols-3 md:grid-cols-4 px-10 lg:px-20 pb-10 mb-5">
        <div className="flex flex-col space-y-4 lg:space-y-6">
          <h2 className="font-semibold pl-3">Hỗ trợ</h2>
          <div className="flex flex-col space-y-3 text-sm dark:text-gray-400">
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Trung tâm trợ giúp
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              AirCover
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Hỗ trợ người khuyết tật
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Các tùy chọn hủy
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Biện pháp ứng phó với đại dịch COVID-19 của chúng tôi
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Báo cáo lo ngại của hàng xóm
            </a>
          </div>
        </div>
        <div className="flex flex-col space-y-4 lg:space-y-6">
          <h2 className="font-semibold pl-3">Cộng đồng</h2>
          <div className="flex flex-col text-sm dark:text-gray-400">
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Airbnb.org: nhà ở cứu trợ
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Chống phân biệt đối xử
            </a>
          </div>
        </div>
        <div className="flex flex-col space-y-4 lg:space-y-6">
          <h2 className="font-semibold pl-3">Đón tiếp khách</h2>
          <div className="flex flex-col space-y-2 text-sm dark:text-gray-400">
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Cho thuê nhà trên Airbnb
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              AirCover cho Chủ nhà
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Xem tài nguyên đón tiếp khách
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Truy cập diễn đàn cộng đồng
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Đón tiếp khách có trách nhiệm
            </a>
          </div>
        </div>
        <div className="flex flex-col space-y-4 lg:space-y-6">
          <h2 className="font-semibold pl-3">Airbnb</h2>
          <div className="flex flex-col text-sm dark:text-gray-400">
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Trang tin tức
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Tìm hiểu các tính năng mới
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Thư ngỏ từ các nhà sáng lập
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Cơ hội nghề nghiệp
            </a>
            <a
              aria-current="page"
              className="nav-link hover:underline hover:text-black active"
              href="/"
            >
              Nhà đầu tư
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
