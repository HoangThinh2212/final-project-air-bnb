import React, { useEffect } from "react";
import Footer from "./Footer";
import FooterLocation from "./FooterLocation";
import FooterNav from "./FooterNav";
import NavSlider from "./NavSlider";
import Header from "./../../Component/Header/Header";
import { useState } from "react";
import { roomService } from "./../../services/roomService";
import RoomList from "./../RoomList/RoomList";

export default function HomePage() {
  const [roomArr, setRoomArr] = useState([]);
  useEffect(() => {
    roomService
      .getRoomList()
      .then((res) => {
        setRoomArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div>
      <Header />
      <NavSlider />
      <div className="container mx-auto">
        <RoomList roomArr={roomArr} />
      </div>

      <FooterLocation />
      <Footer />
      <FooterNav />
    </div>
  );
}
