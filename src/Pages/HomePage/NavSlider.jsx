import React from "react";
import { Swiper, SwiperSlide} from "swiper/react";
import { Autoplay} from "swiper";
import "swiper/css";

export default function NavSlider() {
  return (
    <Swiper
      spaceBetween={1}
      slidesPerView={5}
      loop={true}
      autoplay={{
        delay: 1500,
        disableOnInteraction: false,
      }}
      modules={[Autoplay]}
      className="py-3"
    >

      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/732edad8-3ae0-49a8-a451-29a8010dcc0c.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Cabin</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/a4634ca6-1407-4864-ab97-6e141967d782.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Hồ</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/5ed8f7c7-2e1f-43a8-9a39-4edfc81a3325.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Phục vụ bữa sáng</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/ca25c7f3-0d1f-432b-9efa-b9f5dc6d8770.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Khu cắm trại</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/eb7ba4c0-ea38-4cbb-9db6-bdcc8baad585.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Phòng riêng</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/3b1eb541-46d9-4bef-abc4-c37d77e3c21b.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Khung cảnh tuyệt vời</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/3726d94b-534a-42b8-bca0-a0304d912260.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Được ưa chuộng</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/78ba8486-6ba6-4a43-a56d-f556189193da.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Biệt thự</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/4d4a4eba-c7e4-43eb-9ce2-95e1d200d10e.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Nhà trên cây</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/c8e2ed05-c666-47b6-99fc-4cb6edcde6b4.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Luxe</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/687a8682-68b3-4f21-8d71-3c3aef6c1110.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Thuyền</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/33848f9e-8dd6-4777-b905-ed38342bacb9.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Nhà chỏm nón</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/1b6a8b70-a3b6-48b5-88e1-2243d9172c06.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Lâu đài</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/51f5cf64-5821-400c-8033-8a10c7787d69.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Hanok</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/c0a24c04-ce1f-490c-833f-987613930eca.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Công viên quốc gia</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/8b44f770-7156-4c7b-b4d3-d92549c8652f.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Bắc cực</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/a6dd2bae-5fd0-4b28-b123-206783b5de1d.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Sa mạc</p>
      </SwiperSlide>
      <SwiperSlide className="contrast-0 hover:contrast-100">
        <img
          src="https://a0.muscache.com/pictures/ed8b9e47-609b-44c2-9768-33e6a22eccb2.jpg"
          alt=""
          width={30}
          height={30}
          style={{ margin: "auto" }}
        />
        <p className="font-semibold ">Thành phố nổi tiếng</p>
      </SwiperSlide>
    </Swiper>
  );
}
