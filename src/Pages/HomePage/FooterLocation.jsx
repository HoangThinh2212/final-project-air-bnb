import React from "react";
import pic1 from "../../assets/pic1.jpg";
import pic2 from "../../assets/pic2.jpg";
import pic3 from "../../assets/pic3.jpg";
import pic4 from "../../assets/pic4.jpg";

export default function FooterLocation() {
  return (
    <div className="container grid grid-cols-2 mt-4 mx-auto gap-x-3 gap-y-8 sm:grid-cols-3 md:grid-cols-4 ">
      <div className="flex flex-col">
        <img alt="img" src={pic1} className="rounded-lg" />
        <span className="font-semibold">Toàn bộ nhà</span>
      </div>
      <div className="flex flex-col">
        <img alt="img" src={pic2} className="rounded-lg" />
        <span className="font-semibold">Chỗ ở độc đáo</span>
      </div>
      <div className="flex flex-col">
        <img alt="img" src={pic3} className="rounded-lg" />
        <span className="font-semibold">Trang trại và thiên nhiên</span>
      </div>
      <div className="flex flex-col">
        <img alt="img" src={pic4} className="rounded-lg" />
        <span className="font-semibold">Cho phép mang theo thú cưng</span>
      </div>
    </div>
  );
}
