import React from "react";

export default function RoomByCity() {
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
      <div>List API</div>
      <div class="h-screen w-full hidden md:block sticky top-28">
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62705.365663780416!2d106.63876494416056!3d10.804774629319295!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752fca39e3b493%3A0x865af6a617c57d82!2zVHJ1bmcgVMOibSDEkMOgbyBU4bqhbyBM4bqtcCBUcsOsbmggQ3liZXJTb2Z0IC0gQ8ahIFPhu58gUXXhuq1uIDMsIFRQLkhDTQ!5e0!3m2!1svi!2s!4v1675637060785!5m2!1svi!2s"
          width="600"
          height="450"
          frameBorder="0"
          style={{ border: 0 }}
          allowFullScreen=""
          aria-hidden="false"
          tabIndex="0"
          title="map"
        />
      </div>
    </div>
  );
}
