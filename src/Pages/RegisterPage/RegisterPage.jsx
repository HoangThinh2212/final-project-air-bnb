import React from "react";
import { Button, Form, Input, Select, message, DatePicker } from "antd";
import { userService } from "./../../services/userService";
import { Link, useNavigate } from "react-router-dom";
import background from "../../assets/register_background.jpg";

const { Option } = Select;

export default function RegisterPage() {
  let navigate = useNavigate();
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    userService
      .postDangKy(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng ký thành công");
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Đã có lỗi xảy ra");
        console.log(err);
      });
  };
  const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 10,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 6,
      },
    },
  };

  return (
    <div
      className="w-screen h-screen flex justify-center items-center"
      style={{
        backgroundImage: `url(${background})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
    >
      <div className="container flex relative">
        <div className="bg-white border rounded px-20 pt-1 shadow mx-auto text-center">
          <h1 className="font-semibold text-3xl text-blue-500 py-4">
            Đăng kí tài khoản
          </h1>

          <Form
            {...formItemLayout}
            form={form}
            name="register"
            onFinish={onFinish}
            initialValues={{
              prefix: "079",
            }}
            style={{
              maxWidth: 600,
              fontWeight: "bold",
              fontStyle: "italic",
              
            }}
            
            scrollToFirstError
          >
            <Form.Item
              name="name"
              label="Tên người dùng"
              rules={[
                {
                  required: true,
                  message: "Mời nhập tên người dùng!",
                  whitespace: true,
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="email"
              label="E-mail"
              rules={[
                {
                  type: "email",
                  message: "Vui lòng nhập đúng định dạng example@gmail.com!",
                },
                {
                  required: true,
                  message: "Mời nhập E-mail!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="password"
              label="Mật khẩu"
              rules={[
                {
                  required: true,
                  message: "Mời nhập mật khẩu!",
                },
              ]}
              hasFeedback
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="confirm"
              label="Nhập lại mật khẩu"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Mời xác nhận lại mật khẩu!",
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      new Error("Vui lòng nhập lại mật khẩu đã chọn!")
                    );
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="phone"
              label="Số điện thoại"
              rules={[
                {
                  required: true,
                  message: "Mời nhập số điện thoại!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="gender"
              label="Giới tính"
              rules={[
                {
                  required: true,
                  message: "Hãy chọn giới tính!",
                },
              ]}
            >
              <Select placeholder="Giới tính">
                <Option value="true">True</Option>
              </Select>
            </Form.Item>

            <Form.Item
              name="birthday"
              label="Ngày sinh"
              rules={[
                {
                  required: true,
                  message: "Hãy chọn ngày sinh!",
                },
              ]}
            >
              <DatePicker />
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
              <Button
                className="bg-violet-200 text-red-400 italic"
                htmlType="submit"
              >
                Đăng ký
              </Button>
              <Button className="bg-violet-200 text-red-400 ml-3 italic">
                <Link to="/login" style={{ textDecoration: "none" }}>
                  Đăng nhập
                </Link>
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
