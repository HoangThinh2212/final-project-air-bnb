import React from "react";
import { Card } from "antd";
const { Meta } = Card;

export default function RoomList({ roomArr }) {
  const renderRoomList = () => {
    return roomArr.slice(0, 28).map((item) => {
      return (
        <Card
          hoverable
          style={{
            width: "100%",
          }}
          cover={
            <img
              className="h-52 object-cover"
              alt="example"
              src={item.hinhAnh}
            />
          }
        >
          <Meta
            className="italic"
            title={item.tenPhong}
            description={
              item.moTa.length < 60 ? item.moTa : item.moTa.slice(0, 60) + "..."
            }
          />

          <h3 className="font-semibold text-base">
            Số lượng khách: {item.khach}
          </h3>
          <h3 className="font-semibold text-base">${item.giaTien} đêm</h3>
        </Card>
      );
    });
  };

  return <div className="grid grid-cols-4 gap-5">{renderRoomList()}</div>;
}
